import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  uploadFile: any;
  postId: number;
  options: Object = {
    url: 'http://localhost/dashboard',
    params: { 'post_id': this.postId }
  };

  handleUpload(data): void {
    console.log(data);
    //if (data && data.response) {
      //data = JSON.parse(data.response);
      this.uploadFile = data;
    //}
  }
}
