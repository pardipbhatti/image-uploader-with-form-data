import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgUploaderModule } from 'ngx-uploader';
import {UPLOAD_DIRECTIVES} from 'ng2-file-uploader/ng2-file-uploader';

import { AppComponent } from './app.component';
import { UploaderComponent } from './uploader/uploader.component';
import { UploadService } from './uploader/upload.service';


@NgModule({
  declarations: [
    AppComponent,
    UploaderComponent,
    UPLOAD_DIRECTIVES
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgUploaderModule,
    ReactiveFormsModule
  ],
  providers: [UploadService],
  bootstrap: [AppComponent]
})
export class AppModule { }
