import {Component} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UploadService } from './upload.service';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.css']
})
export class UploaderComponent {


  filesToUpload: Array<File>;
  fields: Array<any>;
  gpUpoader: FormGroup;

  constructor(private fb: FormBuilder, private uploadService: UploadService) {
    this.filesToUpload = [];
    this.gpUpoader = fb.group({
      'gpName': [null, Validators.required],
      'gpEmail': [null, Validators.required]
    });
  }

  upload(data) {
    this.fields = <Array<any>> data;
    this.uploadService.uploadImae(this.fields, this.filesToUpload).then(function (response) {
      console.log(response);
    });
  }

  fileChangeEvent(fileInput: any){
    this.filesToUpload = <Array<File>> fileInput.target.files;
  }

}
