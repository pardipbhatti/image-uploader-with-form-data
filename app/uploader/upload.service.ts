import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UploadService {

  /*
   * Description: Header types
   * Author: Pardip Bhatti (Gagudeep)
   * Date: 27 May 2017
   */
  private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  /*
   * Description: variables declaration
   * Author: Pardip Bhatti (Gagudeep)
   * Date: 27 May 2017
   */
  private url = 'http://localhost/ci/index.php/api/upload';

  /*
   * Description: Constructors
   * Author: Pardip Bhatti (Gagudeep)
   * Date: 27 May 2017
   */
  constructor() { }

  /*
   * Description: Calling upload image and fields functions
   * Author: Pardip Bhatti (Gagudeep)
   * Date: 27 May 2017
   */
  uploadImae(params, files) {
    return this.gpUpload(this.url, params, files);
  }

  /*
   * Description: sending request to server for login
   * Author: Pardip Bhatti (Gagudeep)
   * Date: 27 May 2017
   */
  gpUpload(url: string, params, files: Array<File>) {
    return new Promise((resolve, reject) => {
      const formData: any = new FormData();     // Form data object
      const xhr = new XMLHttpRequest();         // XHR for post data

      // Form fields
      formData.append('gpName', params.gpName);
      formData.append('gpEmail', params.gpEmail);

      // Images append to form data
      for(let i = 0; i < files.length; i++) {
        formData.append('uploads[]', files[i], files[i].name);
      }

      // Response
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open('POST', url, true);
      xhr.send(formData);
    });
  }

}
